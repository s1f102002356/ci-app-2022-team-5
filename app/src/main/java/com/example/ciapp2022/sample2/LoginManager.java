package com.example.ciapp2022.sample2;

import java.util.HashMap;

public class LoginManager {
    private HashMap<String, User> users;

    public LoginManager() {
        this.users = new HashMap<>();
    }

    public void register(String username, String password) throws ValidateFailedException {
        if (!isUsernameValid(username)) {
            throw new ValidateFailedException("Username input is invalid");
        }
        if (!isPasswordValid(password)) {
            throw new ValidateFailedException("Password input is invalid");
        }

        users.put(username, new User(username, password));
    }

    private boolean isUsernameValid(String username) {
        return username.matches("^(?=.*[A-Z])[a-zA-Z0-9]{4,}$");
    }

    private boolean isPasswordValid(String password) {
        boolean checkCapital = false;
        for (int i = 0; i < password.length(); i++) {
            if (Character.isUpperCase(password.charAt(i))) {
                checkCapital = true;
            }
        }
        return (password.matches("^[a-zA-Z0-9]{8,}$") && checkCapital);
    }

    public User login(String username, String password) throws LoginFailedException, UserNotFoundException, InvalidPasswordException {
        if (users.containsKey(username)) {
            if (users.get(username).getPassword().equals(password)) {
                return users.get(username);
            } else {
                throw new InvalidPasswordException("Password not match");
            }
        } else {
            throw new UserNotFoundException("User does not exists");
        }

    }
}
